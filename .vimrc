set nocompatible

so ~/.vim/plugins.vim

"-------------Visual Settings--------------"
colorscheme old-hope
set t_Co=256
let g:netrw_liststyle = 3


"-------------General Settings--------------"
set backspace=indent,eol,start                                          "Make backspace behave like every other editor.
let mapleader = ',' 						    	"The default leader is \, but a comma is much better.
set number								"Let's activate line numbers.
set linespace=15   						        "Macvim-specific line-height.
syntax on
set autowriteall
set complete=.,w,b,u
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab

"-------------Split Management--------------"
set splitbelow 								"Make splits default to below...
set splitright								"And to the right. This feels more natural.

nmap <C-Down> <C-W><C-J>
nmap <C-Up> <C-W><C-K>
nmap <C-Left> <C-W><C-H>
nmap <C-Right> <C-W><C-L>

nmap <Leader>f :tag<space>


"-------------Automations--------------"
function! IPhpInsertUse()
	call PhpInsertUse()
	call feedkeys('a', 'n')
endfunction
autocmd FileType php inoremap <Leader>n <Esc>:call IPhpInsertUse()<cr>
autocmd FileType php noremap <Leader>n :call IPhpInsertUse()<cr>

"-------------Mappings--------------"
" Snippets
nmap <Leader>s :e ~/.vim/snippets/

"
" NERDTree
"
nmap <Leader>a :NERDTreeToggle<cr>

let NERDTreeHijackNetrw = 0

"
" Greplace
"
set grepprg=ag

let g:grep_cmd_opts = '--line-numbers --noheading'


"
" CtrlP
"
nmap <C-R> :CtrlPBufTag<cr>
nmap <C-E> :CtrlPMRUFiles<cr>

let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git'
let g:ctrlp_match_window = 'top,order:ttb,min:1,max:30,results:30'

"
" Emmet
"
let g:user_emmet_leader_key='<Leader>'

"
" Laravel-Specific
"
nmap <Leader>lr :e app/Http/routes.php<cr>
nmap <Leader>lc :CtrlP<cr>app/Http/Controllers/
nmap <Leader>lm :CtrlP<cr>app/
nmap <Leader>lv :e resources/views/<cr>



" Notes and tips
" - zz     -> center the line where the cursor is located
" - ctrl ] -> find the related symbol
" About Vim Surround
" - cs + actual surrounding element + new surrounding element
" - ds + actual surrounding element
" - cst + new tag
" - dst
" Do not forget to run ctags -R --exclude=node_modules|vendor
